class postgres {
  class { 'postgresql::globals':
    encoding => 'UTF-8',
    locale   => 'en_US.UTF-8',
  }

  class { 'postgresql::server':
    listen_addresses => '*',
    ipv4acls => ['host all all 0.0.0.0/0 md5'],
    ipv6acls => ['host all all ::/0 md5'],
    require  => Class['::postgresql::globals'],
  }

  postgresql::server::db { "${project}":
    user     => "${project}",
    password => postgresql::postgresql_password("${project}", 'hitachi'),
  }

  postgresql::server::database_grant { "${project}":
    privilege => 'ALL',
    db        => "${project}",
    role      => "${project}",
    require   => Postgresql::Server::Db["${project}"],
  }

  #service { 'postgresql service':
  #  name    => 'postgresql',
  #  ensure  => running,
  #  require => Class['::postgresql::server'],
  #}

}