class php {
  $enhancers = [ 
    "php${php_version}",
    "php${php_version}-cli",
    "php${php_version}-curl",
    "php${php_version}-dev",
    "php${php_version}-gmp",
    "php${php_version}-gd",
    "php${php_version}-intl",
    "php${php_version}-mbstring",
    "php${php_version}-mysql",
    "php${php_version}-pgsql",
    "php${php_version}-readline",
    "php${php_version}-soap",
    "php${php_version}-sqlite3",
    "php${php_version}-xml",
    "php${php_version}-zip",

    "php${php_version}-fpm",
  ]
  package { $enhancers:
    ensure  => 'installed',
    require => [Apt::Ppa['ppa:ondrej/php'], Exec['apt-get update']],
  }

  # Make sure php8.0-fpm is running
  service { "php${php_version}-fpm":
    ensure => running,
    require => Package["php${php_version}"],
  }

  package { "php${php_version}-xdebug":
    ensure  => 'latest',
    require => Package["php${php_version}"],
  }

  file { 'php.ini copy':
    path => "/etc/php/${php_version}/fpm/php.ini",
    source => "/usr/lib/php/${php_version}/php.ini-development",
    require => Package["php${php_version}"],
  }

  augeas { "php.ini":
    notify  => Service["php${php_version}-fpm"],
    require => [File['php.ini copy'], Package['augeas-tools']],
    context => "/files/etc/php/${php_version}/fpm/php.ini",
    changes => [
      'set PHP/display_errors On',
      'set PHP/display_startup_errors On',
      'set PHP/cgi.fix_pathinfo 0',
      'set PHP/post_max_size 100M',
      'set PHP/upload_max_filesize 100M',
      'set Date/date.timezone Europe/Samara',
      # xdebug 2
      #'set xdebug/zend_extension /usr/lib/php/20200930/xdebug.so',
      #'set xdebug/xdebug.remote_enable 1',
      #'set xdebug/xdebug.remote_connect_back 1',
      # xdebug 3
      'set xdebug/xdebug.mode debug',
      'set xdebug/xdebug.discover_client_host 1',
      #'set xdebug/client_host 192.168.2.1',

      # -1 max depth may require a lot of memory, better be off for certain frameworks
      #'set xdebug/xdebug.var_display_max_depth -1',
      'set xdebug/xdebug.var_display_max_children -1',
      'set xdebug/xdebug.var_display_max_data -1',
    ],
  }
  #8.0 - 20200930, 5.6 - 20131226

  package { 'curl':
    ensure => installed,
  }
 
  exec { 'install composer':
    command => '/usr/bin/curl -sS https://getcomposer.org/installer | php && sudo mv composer.phar /usr/local/bin/composer && sudo chmod 755 /usr/local/bin/composer',
    require => [Package['curl'], Package["php${php_version}"]],
    cwd    => '/vagrant',
    creates => "/usr/local/bin/composer",
    environment => ["HOME=/home/vagrant"],
  }

  file { '/var/www/html/':
    ensure => 'directory',
  }

  file { 'index.php':
    path => '/var/www/html/index.php',
    ensure => present,
    recurse => true,
    content => '<?php phpinfo();',
    require => File['/var/www/html/'],
  }

  file { ['/var/www/html/index.html', '/var/www/html/index.nginx-debian.html']:
    ensure => absent,
  }
}